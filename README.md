# 5BLOC - ELECTION BLOCKCHAIN DAPP

*This repository can also be found on [Gitlab.com](https://gitlab.com/alt4/5bloc-2022)*
*Demonstration uploaded on [Youtube.com](https://youtu.be/AsHcuMRPiYE)*

Election management decentralized application made for 5BLOC. Also contains a Docker-Compose files and assorted instructions to deploy a Geth Ethereum network to run the dapp on!

A chairperson is defined beforehand. Users visit the website and register using their Ethereum accounts.

The chairperson can then:

* Start the election and set an end date
* Accept a registered user
* Give a registered user the right to vote
* Set a registered user as candidate
* End the election prematurely (for testing purposes mostly)
* Arbitrate in the case of a draw between candidates

The voter can see the list of candidates and vote.

The candidate can vote and/or delegate their vote.

All users can check at any time the electoral roll.

When the election is over, anyone can check who won on the Elected page.

## Deployment

### Geth Network

Every resources needed to boot up a Geth network are in the `miners` folder. Instructions are in the README.md file.

### Election App

Once the Geth network is up and running, replace the `network` bloc in `hardhat.config.js` with the following:

```text
module.exports = {
  solidity: "0.8.4",
  paths: {
    artifacts: "./src/blockchain/artifacts",
  },
  networks: {
    prodhost: {
      url: http://<YOUR_RPC_ENDPOINT>:8545,
      accounts: [`<A_PRIVATE_KEY_WITH_FUNDS>`],
      chainId: 7435
    },
  },
};
```

Replace `<YOUR_RPC_ENDPOINT>` with your IP and `<A_PRIVATE_KEY_WITH_FUNDS>` with the private key of an account on your network. This account needs a little bit of funds to pay the initial transaction costs, but will not be used again unless explicitly specified.

Edit `src/blockchain/scripts/deploy.js` to insert the chairperson's address in the function `SmartContract.deploy("CHAIPERSON_ADDRESS")"`, then run:

```bash
npm install
npm run deploy
```

Get the smart contract's address, then paste it in `src/views/index.js` at the const `electionContractAddress`.

Finally, run:

```bash
npm start
```

And after a short compilation time, the application should be up on running on port 3000!
