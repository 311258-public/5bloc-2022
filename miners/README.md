# 5BLOC - Docker-backed Multinode Cluster PoC

## Introduction

This directory contains a few files that can bootstrap a (minimum) 3-nodes Ethereum mining cluster. It is composed of a bootnode, a RPC endpoint and one or more mining nodes.

While it may seem pointless to run all of these on the same rig, it is actually decently easy to migrate from docker-compose to whatever clustering solution one may want to use (~~Swarm~~, Kubernetes, Nomad, etc...).

## Prerequisites

* Docker & docker-compose
* `ethereum/client-go:v1.10.16`

## Setup

* Edit `genesis.json` to your liking (i.e. change the chainID, but remember to change it later as well!)
* Copy `.env.template` to `.env` and edit it to change the `ACCOUNT_PASSWORD` to something robust, as well as the `NETWORKID`, `NODEKEYHEX` and `ENODEHEX` (more information on how to generate the last two just below)
* Run `docker-compose up -d --scale miner=3` or however many miners you want to run (run `docker-compose scale miner=x` later to increase/decrease it)
* Monitor your newly created mining cluster with `docker-compose logs -f`, and in another terminal run a few of the commands depicted in the `Useful API queries` part to make sure the RPC endpoint is working properly

### Generating Nodekey and Enode Hexes (sic)

[Download the Geth & Tools package](https://geth.ethereum.org/downloads/) appropriate to your operating system, unpack it somewhere and run the following commands:

```bash
./bootnode -genkey bootnode.key
cat bootnode.key
./bootnode -nodekey bootnode.key
```

Then immediately abort the last command. Output should be similar to this:

```txt
sol@weinstraub:~/geth-alltools$ ./bootnode -genkey bootnode.key
sol@weinstraub:~/geth-alltools$ cat bootnode.key
724ebbf734fd20a06b8d82d02bc16809d17a2870da5a9a11d46d9b5f3d6fd057
sol@weinstraub:~/geth-alltools$ ./bootnode -nodekey bootnode.key
enode://5988b515e8097dfa0f8f7dabca722f3bd4f4595673de060202ecc6e48ed26f35650f93aba5ee2ff45f9918a119e25c220b3fc761bf1fe984efdf57c204381581@127.0.0.1:0?discport=30301
Note: you're using cmd/bootnode, a developer tool.
We recommend using a regular node as bootstrap node for production deployments.
INFO [02-22|19:13:27.474] New local node record                    seq=1,645,553,607,473 id=96f7ed662a8c23dc ip=<nil> udp=0 tcp=0
^C
```

In the above example, the `NODEKEYHEX` Geth expects is `724ebbf734fd20a06b8d82d02bc16809d17a2870da5a9a11d46d9b5f3d6fd057`, while the `ENODEHEX` is `5988b515e8097dfa0f8f7dabca722f3bd4f4595673de060202ecc6e48ed26f35650f93aba5ee2ff45f9918a119e25c220b3fc761bf1fe984efdf57c204381581` (what's between `enode://` and `@127.0[...]`)

*More informations can be found in the [official tutorial](https://geth.ethereum.org/docs/getting-started/private-net).*

## Useful API queries

Start by creating a var containing your RPC endpoint's hostname to make querying it more convenient:

```bash
ETHRPCENDPOINT=192.168.20.22
```

### Checking if the RPC Node connected to the Bootnode successfully

Run the following:

```bash
curl --location --request POST "$ETHRPCENDPOINT:8545" --header 'Content-Type: application/json' --data-raw '{
    "jsonrpc": "2.0",
    "id": 1,
    "method": "admin_peers",
    "params": []
}'
```

The reply should contain the full enode, and match the one in your docker-compose.

*NOTE: For some reason, it replies empty fields after running for a while. Then again it could be my computer going haywire from running too many miners. In any case, you may want to restart the container.*

### Get the last block number

```bash
curl --location --request POST "$ETHRPCENDPOINT:8545" --header 'Content-Type: application/json' --data-raw '{
    "jsonrpc": "2.0",
    "id": 1,
    "method": "eth_blockNumber",
    "params": []
}'
```

While it will probably display zero for a little bit, the hexadecimal number should climb decently fast as miners do their thing.

### Get the main account address and current funds

```bash
curl --location --request POST "$ETHRPCENDPOINT:8545" --header 'Content-Type: application/json' --data-raw '{
    "jsonrpc": "2.0",
    "id": 1,
    "method": "eth_accounts",
    "params": []
}'
```

Get the hexadecimal value burried inside the returned json (this is your address!), and run the next command (replace `<ADDRESS>` with your own):

```bash
curl --location --request POST "$ETHRPCENDPOINT:8545" --header 'Content-Type: application/json' --data-raw '{
    "jsonrpc": "2.0",
    "id": 1,
    "method": "eth_getBalance",
    "params": [
        "<ADDRESS>",
        "latest"
    ]
}'
```

### Create a new account

You'll need the original password defined in the `.env` file to create a new account.

```bash
curl --location --request POST "$ETHRPCENDPOINT:8545" --header 'Content-Type: application/json' --data-raw '{
    "jsonrpc": "2.0",
    "id": 1,
    "method": "personal_newAccount",
    "params": [
        "<NEWACCOUNTPASSWORD>"
    ]
}'
```

It may be necessary to unlock the account before use, run the following query to do so:

```bash
curl --location --request POST "$ETHRPCENDPOINT:8545" --header 'Content-Type: application/json' --data-raw '{
    "jsonrpc": "2.0",
    "id": 1,
    "method": "personal_unlockAccount",
    "params": [
        "<NEW ACCONT ADDRESS>",
        "<PASSWORD>"
    ]
}'
```

### Transfer funds

It's possible to send funds from one address to another using the following query:

```bash
curl --location --request POST "$ETHRPCENDPOINT:8545" --header 'Content-Type: application/json' --data-raw '{
    "jsonrpc": "2.0",
    "id": 1,
    "method": "eth_sendTransaction",
    "params": [
        {
            "from": "<FROM ADDRESS>",
            "to": "<TO ADDRESS",
            "value": "0xf4240"
        }
    ]
}'
```

It is possible to monitor the transaction status like this:

```bash
curl --location --request POST "$ETHRPCENDPOINT:8545" --header 'Content-Type: application/json' --data-raw '{
    "jsonrpc": "2.0",
    "id": 1,
    "method": "eth_getTransactionByHash",
    "params": [
        {
            "<TRANSACTION ID>"
        }
    ]
}'
```

### Check Sync state after reboot

The RPC node may take a while to synchronize with the bootnode after they're rebooted, to make sure it is syncing properly, run the following query:

```bash
curl --location --request POST "$ETHRPCENDPOINT:8545" --header 'Content-Type: application/json' --data-raw '{
    "jsonrpc": "2.0",
    "id": 1,
    "method": "eth_syncing",
    "params": []
}'
```

## Getting an account's private key

There surely is a better way than this DYI solution, but the method to get the private key from your private network's accounts depicted below requires a bit of finesse.

Make sure you have Python at hand, and install the following lib:

```bash
pip install web3
```

Get your keystore from the RPC endpoint using the following command:

```bash
docker cp miners_rpc-endpoint_1:/root/.ethereum/keystore/ .
```

The keystore should contain several files starting with a timestamp and ending with your account ID.

Run the following Python instructions in the keystore directory:

```python
from web3.auto import w3
with open("UTC--2022-02-22T17-48-19.810290624Z--20f483f758a58d02a969b636c02f6c2e6bfc941c") as keyfile:
    encrypted_key = keyfile.read()
    private_key = w3.eth.account.decrypt(encrypted_key, '<PASSWORD>')
private_key
```

Replace the `UTC[...]941c` string with your own keystore file name and fill the password appropriately to get the account's private key.
