// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

contract ElectionContract {
    struct Voter {
        string role; // voter, chairmain or candidate
        address accountAddress; // Address of account
        bool canVote; // right to vote one time
        bool voted; // if true, that person already voted
        uint256 nbrVote; // index of the voted proposal
        address voteReceiver; // Candidate delegated to if voter's role is candidate or candidate voted for if voter's role is voter
        bool delegated;
        bool isValue; // helps to verify key in map before setting it to handle update when create
    }

    struct Election {
        uint256 dateOver; // deadline of the election process, unix timestamp
    }

    address public chairperson;

    mapping(address => Voter) public voters;

    address[] public candidates;

    Election public election;

    Voter[] public potentielWinners;

    address[] public registered;

    constructor(address chairPersonAddress) {
        chairperson = chairPersonAddress;
        setVoter(chairPersonAddress, "CHAIRPERSON");
        registered.push(chairPersonAddress);
        election.dateOver = 0;
    }

    function setVoters(address[] memory votersAddresses) public returns (bool) {
        for (uint256 i = 0; i < votersAddresses.length; i++) {
            if (!voters[votersAddresses[i]].isValue) {
                setVoter(votersAddresses[i], "VOTER");
            }
        }
        return true;
    }

    function setVoter(address voterAddress, string memory role)
        private
        returns (bool)
    {
        voters[voterAddress] = Voter({
            role: role,
            accountAddress: voterAddress,
            canVote: compareStrings(role, "CHAIRPERSON"),
            voted: false,
            nbrVote: 0,
            voteReceiver: address(0),
            delegated: false,
            isValue: true
        });
        return true;
    }

    function getRegister() public view returns (address[] memory) {
        return registered;
    }

    function setRegister(address registerAddress) public {
        require(!checkRegistred(registerAddress), "Already registred.");
        registered.push(registerAddress);
    }

    function checkRegistred(address registerAddress)
        internal
        view
        returns (bool)
    {
        for (uint256 i = 0; i < registered.length; i++) {
            if (registered[i] == registerAddress) return true;
        }
        return false;
    }

    function getVoter(address voterAddress) public view returns (Voter memory) {
        return voters[voterAddress];
    }

    function setRightToVote(address voterAddress) public {
        require(
            msg.sender == chairperson,
            "Only chairperson can give right to vote."
        );
        require(!voters[voterAddress].voted, "The voter already voted.");
        require(!voters[voterAddress].canVote);
        voters[voterAddress].canVote = true;
    }

    function setCandidate(address voterAddress) public {
        require(
            msg.sender == chairperson,
            "Only chairperson can set candidates."
        );
        require(voters[voterAddress].isValue, "Voter does not exists.");
        require(
            !compareStrings(voters[voterAddress].role, "CANDIDATE"),
            "Voter is already candidate."
        );
        require(
            !compareStrings(voters[voterAddress].role, "CHAIRMAN"),
            "Voter is chairman."
        );
        require(!voters[voterAddress].voted, "The voter already voted.");
        voters[voterAddress].role = "CANDIDATE";
        candidates.push(voterAddress);
    }

    function compareStrings(string memory a, string memory b)
        internal
        pure
        returns (bool)
    {
        return keccak256(abi.encodePacked(a)) == keccak256(abi.encodePacked(b));
    }

    function setElectionEnd(uint256 timestamp) public {
        require(
            msg.sender == chairperson,
            "Only chairperson can set the election end date."
        );
        require(election.dateOver == 0, "Election end was already set.");
        election.dateOver = timestamp;
    }

    function forceEndElection() public {
        require(
            msg.sender == chairperson,
            "Only chairperson can set the election end date."
        );
        require(election.dateOver != 0, "Election end was already set.");
        require(block.timestamp < election.dateOver, "Election is closed.");
        election.dateOver = block.timestamp;
        endElection();
    }

    function getElectionEnd() public view returns (uint256) {
        return election.dateOver;
    }

    function delegate(address to) public {
        Voter storage sender = voters[msg.sender];
        Voter storage receiver = voters[to];
        require(
            compareStrings(sender.role, "CANDIDATE") &&
                compareStrings(receiver.role, "CANDIDATE"),
            "Voter and receiver must be candidates."
        );
        require(to != msg.sender, "Self-delegation is disallowed.");
        require(!receiver.delegated, "Receiver already delegated votes.");
        require(block.timestamp < election.dateOver, "Election is closed.");

        sender.voteReceiver = to;
        sender.delegated = true;
        sender.voted = true;
        if (sender.canVote) {
            receiver.nbrVote = sender.nbrVote + 1; // Add sender number of vote and 1 right to vote
            sender.canVote = false;
        } else {
            receiver.nbrVote = sender.nbrVote; // Add sender number of vote and 1 right to vote
        }
    }

    function vote(address to) public {
        Voter storage sender = voters[msg.sender];
        Voter storage receiver = voters[to];
        require(sender.canVote, "Has no right to vote.");
        require(!sender.voted, "Already voted.");
        require(
            compareStrings(receiver.role, "CANDIDATE"),
            "Receiver is not a candidate."
        );
        require(block.timestamp < election.dateOver, "Election is closed.");
        require(
            msg.sender != chairperson,
            "Chairperson can only arbitrate if necessary."
        );
        sender.voted = true;
        sender.canVote = false;
        sender.voteReceiver = to;

        receiver.nbrVote += 1;
    }

    function getVoteMax() internal view returns (uint256) {
        uint256 nbrVoteMax = 0;
        for (uint256 i = 0; i < candidates.length; i++) {
            if (compareStrings(voters[candidates[i]].role, "CANDIDATE")) {
                if (voters[candidates[i]].nbrVote > nbrVoteMax) {
                    nbrVoteMax = voters[candidates[i]].nbrVote;
                }
            }
        }
        return nbrVoteMax;
    }

    function getPotentielWinners() internal {
        uint256 nbrVoteMax = getVoteMax();
        for (uint256 i = 0; i < candidates.length; i++) {
            if (compareStrings(voters[candidates[i]].role, "CANDIDATE")) {
                if (voters[candidates[i]].nbrVote == nbrVoteMax) {
                    potentielWinners.push(voters[candidates[i]]);
                }
            }
        }
    }

    function endElection() public {
        require(election.dateOver != 0, "Election has not started yet.");
        require(candidates.length > 0, "Election has no candidates.");
        require(
            block.timestamp >= election.dateOver,
            "Election is in progress."
        );
        delete potentielWinners;
        getPotentielWinners();
    }

    function getWinner() public view returns (Voter[] memory) {
        require(election.dateOver != 0, "Election has not started yet.");
        require(candidates.length > 0, "Election has no candidates.");
        require(
            block.timestamp >= election.dateOver,
            "Election is in progress."
        );
        return potentielWinners;
    }

    function arbitrate(address winnerAddress) public {
        require(election.dateOver != 0, "Election has not started yet.");
        require(
            block.timestamp >= election.dateOver,
            "Election is in progress."
        );
        require(potentielWinners.length > 1, "Arbitration not necessary");
        require(msg.sender == chairperson, "Only chairperson can arbitrate.");
        require(voters[msg.sender].canVote, "Chairperson can arbitrate once.");
        voters[winnerAddress].nbrVote += 1;
        voters[msg.sender].canVote = false;
        endElection();
    }
}
