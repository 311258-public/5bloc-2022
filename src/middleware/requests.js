// Format Voter Object
exports.formatVoterObject = (brutData) => {
  return {
    role: brutData.role,
    accountAddress: brutData.accountAddress,
    canVote: brutData.canVote,
    voted: brutData.voted,
    nbrVote: brutData.nbrVote.toNumber(),
    voteReceiver: brutData.voteReceiver,
    isValue: brutData.isValue,
  };
};
// Is the account address eligible to vote
exports.canVote = (account) => {
  return account.canVote && !account.voted;
};
