function Menu_element(title, href, children) {
  this.title = title;
  this.href = href;
  this.children = children;
}

export const menu = [new Menu_element("Adresses", "/address", []), new Menu_element("Elected", "/elected", [])];
