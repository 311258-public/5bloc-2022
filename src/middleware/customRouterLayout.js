import { Route, Redirect, Switch } from "react-router-dom";

import Layout from "./../components/layout";

export default function CustomRouterLayout({ routes, profil, setElectionEndRequest, forceEndElectionRequest, getElectionEndRequest }) {
  return (
    <Layout
      profil={profil}
      setElectionEndRequest={setElectionEndRequest}
      forceEndElectionRequest={forceEndElectionRequest}
      getElectionEndRequest={getElectionEndRequest}
    >
      <Switch>
        {Object.keys(routes).map((page, i) => (
          <Route key={i} exact={routes[page].is_exact} path={routes[page].path}>
            {routes[page].comp}
          </Route>
        ))}
        <Redirect to="/" />
      </Switch>
    </Layout>
  );
}
