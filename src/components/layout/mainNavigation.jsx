import { useState, useEffect } from "react";
import { Navbar, Container, Nav } from "react-bootstrap";
import { PencilSquare } from "react-bootstrap-icons";
import { LinkContainer } from "react-router-bootstrap";

import { menu } from "./../../middleware/menu";
import { canVote } from "./../../middleware/requests";
import ModalComp from "./../el_comp/modalComp";

export default function MainNavigation({ profil, setElectionEndRequest, forceEndElectionRequest, getElectionEndRequest }) {
  const [modalShow, setModalShow] = useState(false);
  const [ElectionEndDate, setElectionEndDate] = useState(null);
  const [IntervalCounter, setIntervalCounter] = useState(null);

  function startTimer(timestamp) {
    var countDownDate = new Date(timestamp).getTime();

    if (timestamp === 0) {
      document.getElementById("timer").innerHTML = "Démarer éléction";
    } else {
      return setInterval(function () {
        const now = new Date().getTime();

        const distance = countDownDate - now;

        const days = Math.floor(distance / (1000 * 60 * 60 * 24));
        const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        const seconds = Math.floor((distance % (1000 * 60)) / 1000);

        document.getElementById("timer").innerHTML = days + "d " + hours + "h " + minutes + "m " + seconds + "s ";

        if (distance < 0) {
          clearInterval(this);
          document.getElementById("timer").innerHTML = "EXPIRED";
        }
      }, 1000);
    }
  }

  async function setElectionEnd(timestamp) {
    await setElectionEndRequest(timestamp);
    fetchElectionDate();
  }

  async function forceEndElection() {
    await forceEndElectionRequest();
    fetchElectionDate();
  }

  async function fetchElectionDate() {
    setElectionEndDate((await getElectionEndRequest()).toNumber());
  }

  function handleModalView() {
    if (profil.role === "CHAIRPERSON") {
      setModalShow(true);
    }
  }

  useEffect(() => {
    fetchElectionDate();
    return () => {
      if (IntervalCounter) clearInterval(IntervalCounter);
    };
  }, []);

  useEffect(() => {
    if (IntervalCounter) clearInterval(IntervalCounter);
    if (ElectionEndDate) setIntervalCounter(startTimer(ElectionEndDate));
  }, [ElectionEndDate]);

  function get_links() {
    return (
      <Nav className="me-auto">
        {menu.map((element, i) => (
          <LinkContainer key={i} to={element.href}>
            <Nav.Link>{element.title}</Nav.Link>
          </LinkContainer>
        ))}
      </Nav>
    );
  }
  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Container>
        <LinkContainer to="/">
          <Navbar.Brand>SupElection</Navbar.Brand>
        </LinkContainer>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">{get_links()}</Navbar.Collapse>
        {profil ? (
          <Nav>
            <Nav.Link disabled style={{ display: "flex", verticalAlign: "baseline" }}>
              <h5>{profil.accountAddress}</h5>
              <div>
                <PencilSquare style={{ marginLeft: "6px" }} size={18} color={canVote(profil) ? "green" : "red"} />
              </div>
            </Nav.Link>
          </Nav>
        ) : null}
        <Nav>
          <Nav.Link onClick={() => handleModalView()}>
            <span id="timer" style={{ verticalAlign: "baseline" }}>
              Début d'éléction
            </span>
          </Nav.Link>
        </Nav>
      </Container>
      <ModalComp show={modalShow} onHide={() => setModalShow(false)} setElectionEnd={setElectionEnd} forceEndElection={forceEndElection} />
    </Navbar>
  );
}
