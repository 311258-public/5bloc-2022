import { Row, Col } from "react-bootstrap";

import MainNavigation from "./mainNavigation";

export default function Layout({ children, profil, setElectionEndRequest, forceEndElectionRequest, getElectionEndRequest }) {
  return (
    <>
      <Row className="px-0 mx-0">
        <MainNavigation
          profil={profil}
          setElectionEndRequest={setElectionEndRequest}
          forceEndElectionRequest={forceEndElectionRequest}
          getElectionEndRequest={getElectionEndRequest}
        />
      </Row>
      <Row className="justify-content-center mt-4 px-0 mx-0">
        <Col lg={10}> {children} </Col>
      </Row>
    </>
  );
}
