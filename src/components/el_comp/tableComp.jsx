import React from "react";
import { Table, Button } from "react-bootstrap";
import { PersonPlusFill, PencilSquare, PersonLinesFill, CheckLg, ClockHistory, DashLg, XLg } from "react-bootstrap-icons";

export default function TableComp({ isChairPerson, accounts, setVotersRequest, setRightToVoteRequest, setCandidateRequest }) {
  const handleNotVoter = (condition, result) => {
    if (condition) return result;
    return <DashLg />;
  };

  return (
    <Table striped bordered hover responsive>
      <thead style={{ textAlign: "center" }}>
        <tr>
          <th>Address</th>
          <th>Role</th>
          <th>Peut voter</th>
          <th>A Voté</th>
          <th>Nbr de votes</th>
          {isChairPerson ? <th>Actions</th> : null}
        </tr>
      </thead>
      <tbody>
        {accounts.map((element, i) => (
          <tr key={i}>
            <td style={{ maxWidth: "160px", textAlign: "center" }}>{element.accountAddress}</td>
            <td style={{ maxWidth: "60px", textAlign: "center" }}>{handleNotVoter(element.isValue, element.role)}</td>
            <td style={{ maxWidth: "60px", textAlign: "center" }}>
              {handleNotVoter(element.isValue, element.canVote ? <CheckLg color="green" /> : <XLg color="red" />)}
            </td>
            <td style={{ maxWidth: "60px", textAlign: "center" }}>
              {handleNotVoter(element.isValue, element.voted ? <CheckLg /> : <ClockHistory />)}
            </td>
            <td style={{ maxWidth: "60px", textAlign: "center" }}>{handleNotVoter(element.isValue, element.nbrVote)}</td>
            {isChairPerson ? (
              <td style={{ maxWidth: "60px", textAlign: "center" }}>
                <Button disabled={element.role === "CHAIRPERSON"} variant="primary" onClick={(e) => setVotersRequest([element.accountAddress])}>
                  <PersonPlusFill />
                </Button>
                <Button
                  disabled={element.role === "CHAIRPERSON"}
                  variant="secondary"
                  style={{ marginLeft: "2px" }}
                  onClick={(e) => setRightToVoteRequest(element.accountAddress)}
                >
                  <PencilSquare />
                </Button>
                <Button
                  disabled={element.role === "CHAIRPERSON"}
                  variant="success"
                  style={{ marginLeft: "2px" }}
                  onClick={(e) => setCandidateRequest(element.accountAddress)}
                >
                  <PersonLinesFill />
                </Button>
              </td>
            ) : null}
          </tr>
        ))}
      </tbody>
    </Table>
  );
}
