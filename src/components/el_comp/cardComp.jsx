import { Col, Card, Button } from "react-bootstrap";
import { EyeFill, PencilSquare } from "react-bootstrap-icons";

export default function CardComp({ candidat, isCandidate, isEligible, isSelf, voteRequest, delegateRequest }) {
  return (
    <Col lg={3}>
      <Card style={{ width: "18rem" }}>
        <Card.Img
          variant="top"
          src="https://cdn.radiofrance.fr/s3/cruiser-production/2021/03/167740c1-0371-4a94-8bdc-e35684190215/1136_montage_ok.jpg"
        />
        <Card.Body>
          <Card.Title>{candidat.accountAddress}</Card.Title>
          <div>
            <Button variant="secondary" disabled={!isEligible} onClick={(e) => voteRequest(candidat.accountAddress)}>
              <PencilSquare size={22} style={{ marginRight: "2px" }} />
              Vote
            </Button>
            {isCandidate && !isSelf ? (
              <Button variant="primary" onClick={(e) => delegateRequest(candidat.accountAddress)}>
                <EyeFill size={22} style={{ marginRight: "2px" }} />
                Delegate
              </Button>
            ) : null}
          </div>
        </Card.Body>
      </Card>
    </Col>
  );
}
