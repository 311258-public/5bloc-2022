import { Modal, Button, Row, Col, Form } from "react-bootstrap";

export default function ModalComp({ show, onHide, setElectionEnd, forceEndElection }) {
  return (
    <Modal show={show} onHide={onHide} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">Démarrer les éléctions</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Row style={{ minHeight: "120px" }}>
          <Col lg={6} style={{ padding: "12px" }}>
            <Form.Group controlId="dateEnd">
              <Form.Label> Date de fin des éléctions: </Form.Label>
              <Form.Control type="date" name="duedate" placeholder="Due date" />
            </Form.Group>
            <div style={{ textAlign: "center", marginTop: "10px" }}>
              <Button variant="success" onClick={(e) => setElectionEnd(new Date(document.getElementById("dateEnd").value).getTime())}>
                Confirmer
              </Button>
            </div>
          </Col>
          <Col lg={6} style={{ padding: "12px", textAlign: "center", margin: "auto" }}>
            <Button variant="danger" onClick={(e) => forceEndElection()}>
              Forcer l'arret des éléctions!
            </Button>
          </Col>
        </Row>
      </Modal.Body>
    </Modal>
  );
}
