import { Card, Row, Button } from "react-bootstrap";

import TableComp from "./../../el_comp/tableComp";

export default function Address(props) {
  return (
    <Card className="homeCards">
      <Card.Header as="h4" className="blue-heading">
        Liste des adresses
        <Button variant="success" style={{ float: "right" }} onClick={() => props.handleRegistration()}>
          Inscription
        </Button>
      </Card.Header>

      <Card.Body>
        <Row>
          <TableComp {...props} />
        </Row>
      </Card.Body>
    </Card>
  );
}
