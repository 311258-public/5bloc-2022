import { Card, Row } from "react-bootstrap";

import { canVote } from "./../../../middleware/requests";
import CardComp from "./../../el_comp/cardComp";

export default function LandingPage({ profil, candidates, delegateRequest, voteRequest }) {
  return (
    <Card className="homeCards">
      <Card.Header as="h4" className="blue-heading">
        Liste des candidats
      </Card.Header>

      <Card.Body style={{ padding: "20px", paddingBottom: "20px" }}>
        <Row style={{ marginBottom: "20px" }}>
          {candidates.map((candidat, i) => (
            <CardComp
              key={i}
              candidat={candidat}
              isCandidate={profil.role === "CANDIDATE"}
              isEligible={canVote(profil)}
              isSelf={candidat.accountAddress === profil.accountAddress}
              delegateRequest={delegateRequest}
              voteRequest={voteRequest}
            />
          ))}
        </Row>
      </Card.Body>
    </Card>
  );
}
