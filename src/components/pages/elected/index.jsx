import { Card, Row, Col, Button } from "react-bootstrap";
import { PencilSquare } from "react-bootstrap-icons";

export default function Elected({ isChairPerson, voteForWinner, winners = [] }) {
  return (
    <Card className="homeCards">
      <Card.Header as="h4" className="blue-heading">
        Liste des élus
      </Card.Header>

      <Card.Body>
        <Row style={{ justifyContent: "center" }}>
          {winners.map((winner, i) => (
            <Col lg={3} key={i}>
              <Card style={{ width: "18rem" }}>
                <Card.Img
                  variant="top"
                  src="https://cdn.radiofrance.fr/s3/cruiser-production/2021/03/167740c1-0371-4a94-8bdc-e35684190215/1136_montage_ok.jpg"
                />
                <Card.Body>
                  <Card.Title>{winner.accountAddress}</Card.Title>
                  {winners.length > 1 && isChairPerson ? (
                    <Button variant="secondary" onClick={(e) => voteForWinner(winner.accountAddress)}>
                      <PencilSquare size={22} style={{ marginRight: "2px" }} />
                      VETO
                    </Button>
                  ) : null}
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
      </Card.Body>
    </Card>
  );
}
