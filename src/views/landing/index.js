import LandingPage from "./../../components/pages/landingpage";

export default function Landing({ profil, accounts, delegateRequest, voteRequest }) {
  return (
    <LandingPage
      profil={profil}
      candidates={accounts.filter((account) => account.role === "CANDIDATE")}
      voteRequest={voteRequest}
      delegateRequest={delegateRequest}
    />
  );
}
