import { useState, useEffect } from "react";
import { BrowserRouter } from "react-router-dom";
import { ethers } from "ethers";

import CustomRouterLayout from "./../middleware/customRouterLayout";
import { LoadingComp } from "./../middleware/components";
import { formatVoterObject } from "./../middleware/requests";
import Landing from "./landing";
import Address from "./address";
import Elected from "./elected";

import ElectionContract from "./../blockchain/artifacts/contracts/ElectionContract.sol/ElectionContract.json";

const electionContractAddress = "0x9fE46736679d2D9a65F0992F2272dE9f3c7fa6e0";

export default function Index() {
  const [Loading, setLoading] = useState(true);
  const [Profil, setProfil] = useState(0);
  const [Accounts, setAccounts] = useState([]);

  async function getSignerContract() {
    const provider = new ethers.providers.Web3Provider(window.ethereum);
    return new ethers.Contract(electionContractAddress, ElectionContract.abi, provider.getSigner());
  }

  async function getProviderContract() {
    const provider = new ethers.providers.Web3Provider(window.ethereum);
    return new ethers.Contract(electionContractAddress, ElectionContract.abi, provider);
  }

  async function ethAuthentication() {
    const currentAddress = await window.ethereum.request({ method: "eth_requestAccounts" });
    const currentVoter = await (await getProviderContract()).getVoter(currentAddress[0]);
    let currentAccount = formatVoterObject(currentVoter);
    if (!currentAccount.isValue) currentAccount.accountAddress = currentAddress[0];
    setProfil(currentAccount);
    await fetchData();
  }

  async function setVotersRequest(votersAddress) {
    const transaction = await (await getSignerContract()).setVoters(votersAddress);
    await transaction.wait();
    await fetchData();
  }

  async function setRegisterRequest(registerAddress) {
    const transaction = await (await getSignerContract()).setRegister(registerAddress);
    await transaction.wait();
    await fetchData();
  }

  async function setCandidateRequest(voterAddress) {
    const transaction = await (await getSignerContract()).setCandidate(voterAddress);
    await transaction.wait();
    await fetchData();
  }

  async function setRightToVoteRequest(voterAddress) {
    await (await getSignerContract()).setRightToVote(voterAddress);
    await fetchData();
  }

  async function setElectionEndRequest(timestamp) {
    const transaction = await (await getSignerContract()).setElectionEnd(timestamp);
    await transaction.wait();
    await fetchData();
  }

  async function forceEndElectionRequest() {
    const transaction = await (await getSignerContract()).forceEndElection();
    await transaction.wait();
    await fetchData();
  }

  async function delegateRequest(addressTo) {
    const transaction = await (await getSignerContract()).delegate(addressTo);
    await transaction.wait();
    await fetchData();
  }

  async function voteRequest(addressTo) {
    const transaction = await (await getSignerContract()).vote(addressTo);
    await transaction.wait();
    await fetchData();
  }

  async function arbitrateRequest(addressTo) {
    const transaction = await (await getSignerContract()).arbitrate(addressTo);
    await transaction.wait();
    await fetchData();
  }

  async function getVoterRequest(voterAddress) {
    return await (await getProviderContract()).getVoter(voterAddress);
  }

  async function getRegisterRequest() {
    return await (await getProviderContract()).getRegister();
  }

  async function getWinnerRequest() {
    return await (await getProviderContract()).getWinner();
  }

  async function getElectionEndRequest() {
    return await (await getProviderContract()).getElectionEnd();
  }

  async function fetchData() {
    const registerAddresses = await getRegisterRequest();
    let accounts = [];
    for (let i = 0; i < registerAddresses.length; i++) {
      const address = registerAddresses[i];
      let currentAccount = formatVoterObject(await getVoterRequest(address));
      if (!currentAccount.isValue) currentAccount.accountAddress = address;
      accounts.push(currentAccount);
    }
    setAccounts(accounts);
  }

  useEffect(() => {
    setLoading(true);
    if (typeof window.ethereum !== "undefined") {
      ethAuthentication();
      window.ethereum.on("accountsChanged", function () {
        ethAuthentication();
      });
    }
    setLoading(false);
  }, []);

  if (Loading) return <LoadingComp />;

  return (
    <BrowserRouter>
      <CustomRouterLayout
        profil={Profil}
        setElectionEndRequest={setElectionEndRequest}
        forceEndElectionRequest={forceEndElectionRequest}
        getElectionEndRequest={getElectionEndRequest}
        routes={[
          {
            is_exact: true,
            path: "/",
            comp: <Landing profil={Profil} accounts={Accounts} delegateRequest={delegateRequest} voteRequest={voteRequest} />,
          },
          {
            is_exact: true,
            path: "/address",
            comp: (
              <Address
                profil={Profil}
                accounts={Accounts}
                getVoterRequest={getVoterRequest}
                setVotersRequest={setVotersRequest}
                setRightToVoteRequest={setRightToVoteRequest}
                setCandidateRequest={setCandidateRequest}
                setRegisterRequest={setRegisterRequest}
              />
            ),
          },
          {
            is_exact: true,
            path: "/elected",
            comp: <Elected profil={Profil} arbitrateRequest={arbitrateRequest} getWinnerRequest={getWinnerRequest} />,
          },
        ]}
      />
    </BrowserRouter>
  );
}
