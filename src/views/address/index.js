import Address from "./../../components/pages/address";

export default function AddressIndex({ profil, accounts, setVotersRequest, setRightToVoteRequest, setCandidateRequest, setRegisterRequest }) {
  const handleRegistration = async () => {
    await setRegisterRequest(profil.accountAddress);
  };

  return (
    <Address
      isChairPerson={profil.role === "CHAIRPERSON"}
      accounts={accounts}
      setVotersRequest={setVotersRequest}
      setRightToVoteRequest={setRightToVoteRequest}
      setCandidateRequest={setCandidateRequest}
      handleRegistration={handleRegistration}
    />
  );
}
