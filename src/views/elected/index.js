import { useState, useEffect } from "react";
import Elected from "./../../components/pages/elected";
import { LoadingComp } from "./../../middleware/components";
import { formatVoterObject } from "./../../middleware/requests";

export default function ElectedIndex({ profil, arbitrateRequest, getWinnerRequest }) {
  const [Loading, setLoading] = useState(true);
  const [Winners, setWinners] = useState([]);

  const voteForWinner = async (addressTo) => {
    await arbitrateRequest(addressTo);
    fetchWinners();
  };

  const fetchWinners = async () => {
    let winners = [];
    const tempWinners = await getWinnerRequest();
    for (let i = 0; i < tempWinners.length; i++) {
      const winner = tempWinners[i];
      winners.push(formatVoterObject(winner));
    }
    setWinners(winners);
  };

  useEffect(() => {
    setLoading(true);
    fetchWinners();
    setLoading(false);
  }, []);

  if (Loading) return <LoadingComp />;

  return <Elected isChairPerson={profil.role === "CHAIRPERSON"} voteForWinner={voteForWinner} winners={Winners} />;
}
